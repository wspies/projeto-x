<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLifestyleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('lifestyle')) {
              Schema::create('lifestyle', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('patient_id')->unsigned();
                $table->boolean('vegetable_fruit');
                $table->boolean('feijao');
                $table->boolean('red_meat');
                $table->boolean('whole_milk');
                $table->boolean('physical_activity');
                $table->boolean('commuting_activity');
                $table->boolean('without_activity');
                $table->boolean('tv_3_hours');
                $table->boolean('regular_alcohol');
                $table->smallInteger('bmi');
                $table->ENUM('smoker',['Y','N','E']);
                $table->smallInteger('smoker_sigaretes_day');
                $table->smallInteger('smoker_years');
                $table->timestamps();
            });
        }

         if (!Schema::hasTable('lifestyle_activity')) {
             Schema::create('lifestyle_activity', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('lifestyle_id')->unsigned();
                $table->string('activity_description',255);
                $table->tinyInteger('activity_weekly_frequency')->unsigned();
                $table->timestamps();
            });  
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lifestyle_activity');
        Schema::drop('lifestyle');
    }
}
