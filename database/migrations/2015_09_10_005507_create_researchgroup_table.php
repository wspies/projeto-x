<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResearchgroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('researchgroup')) 
        {
             Schema::create('researchgroup', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->text('notes');
                $table->timestamps();
            });
        }

        if (!Schema::hasTable('diagnose_researchgroup')) 
        {    
            Schema::create('diagnose_researchgroup', function (Blueprint $table) {
                $table->integer('diagnose_id')->unsigned()->index();
                $table->integer('researchgroup_id')->unsigned()->index();
                $table->timestamps();
            });
        } 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('researchgroup');
    }
}
