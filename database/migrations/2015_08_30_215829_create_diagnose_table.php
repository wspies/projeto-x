<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiagnoseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if (!Schema::hasTable('diagnose')) {
             Schema::create('diagnose', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('patient_id')->unsigned();
                $table->text('condition');
                $table->text('notes');
                $table->timestamps();
            });
        }
        
        if (!Schema::hasTable('diagnose_entity_condition')) {     
            Schema::create('diagnose_entity_condition', function (Blueprint $table) {
                $table->integer('diagnose_id')->unsigned()->index();
                $table->integer('enitity_condition_id')->unsigned()->index();
                $table->timestamps();
            }); 
         }
         
        if (!Schema::hasTable('diagnose_enity_dsm')) {    
            Schema::create('diagnose_entity_dsm', function (Blueprint $table) {
                $table->integer('diagnose_id')->unsigned()->index();
                $table->integer('entity_dsm_id')->unsigned()->index();
                $table->timestamps();
            }); 
        }
        if (!Schema::hasTable('diagnose_entity_special_need')) {
            Schema::create('diagnose_entity_special_need', function (Blueprint $table) {
                $table->integer('diagnose_id')->unsigned()->index();
                $table->integer('entity_special_need_id')->unsigned()->index();
                $table->timestamps();
            }); 
        }
        
        if (!Schema::hasTable('diagnose_entity_relative')) {
            Schema::create('diagnose_enity_relative', function (Blueprint $table) {
                $table->integer('diagnose_id')->unsigned()->index();
                $table->integer('entity_relative_id')->unsigned()->index();
                $table->timestamps();
            }); 
        }   
        
        if (!Schema::hasTable('diagnose_medicine')) {    
            Schema::create('diagnose_medicine', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('diagnose_id')->unsigned()->index();
                $table->string('medicine_name');
                $table->string('medicine_dose');
                $table->string('medicine_observation');
                $table->timestamps();
            }); 
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::drop('diagnose_entity_condition');
        Schema::drop('diagnose_enity_dsm');
        Schema::drop('diagnose_special_need');
        Schema::drop('diagnose_history');
        Schema::drop('diagnose_medicine');
        Schema::drop('diagnose');
    }
}
