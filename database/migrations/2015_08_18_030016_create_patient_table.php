<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       if (!Schema::hasTable('patient')) {
           Schema::create('patient', function (Blueprint $table) {
                $table->increments('id');
                $table->string('firstname');
                $table->string('middlename');
                $table->string('lastname');
                $table->enum('gender',['F','M']);
                $table->date('birthday');    
                $table->string('identifier');    
                $table->string('address1');
                $table->string('address2');
                $table->string('zipcode');
                $table->string('city');
                $table->string('neighborhood');
                $table->boolean('urbanarea');
                $table->integer('telephone');
                $table->integer('mobile');
                $table->string('email')->unique();
                $table->string('profession');
                $table->string('education');
                $table->text('notes');
                $table->string('therapist');
                $table->string('clinic');
               
                $table->rememberToken();
                $table->timestamps();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::drop('patient');
    }
}
