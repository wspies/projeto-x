<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDiagnoseTableFK extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('diagnose', function($table)
            {
            $table->foreign('patient_id')->references('id')->on('patient')->onDelete('cascade');
            });

        Schema::table('diagnose_condition', function($table)
            {
            $table->foreign('diagnose_id')->references('id')->on('diagnose')->onDelete('cascade');
            });

        Schema::table('diagnose_dsm', function($table)
            {
            $table->foreign('diagnose_id')->references('id')->on('diagnose')->onDelete('cascade');
            });

        Schema::table('diagnose_special_need', function($table)
            {
            $table->foreign('diagnose_id')->references('id')->on('diagnose')->onDelete('cascade');
            });

        Schema::table('diagnose_history', function($table)
            {
            $table->foreign('diagnose_id')->references('id')->on('diagnose')->onDelete('cascade');
            });

        Schema::table('diagnose_medicine', function($table)
            {
            $table->foreign('diagnose_id')->references('id')->on('diagnose')->onDelete('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
