<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLifestyleTableFK extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        try{
            Schema::table('lifestyle', function($table)
                {
                $table->foreign('patient_id')->references('id')->on('patient')->onDelete('cascade');
                });
            }
        catch(Exception $e){}
         

        try{
            Schema::table('lifestyle_activity', function($table)
                {
                $table->foreign('lifestyle_id')->references('id')->on('lifestyle')->onDelete('cascade');
                   });
            }
        catch(Exception $e){}

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
