<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntityValueTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          if (!Schema::hasTable('entity_condition')) {     
            Schema::create('entity_condition', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
            }); 
         }

        if (!Schema::hasTable('entity_dsm')) {     
            Schema::create('entity_dsm', function (Blueprint $table) {
                $table->increments('id');
                $table->string('code');
                $table->string('name');
            }); 
         }

        if (!Schema::hasTable('entity_special_needs')) {     
            Schema::create('entity_special_needs', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
            }); 
         } 

         if (!Schema::hasTable('entity_relatives')) {     
            Schema::create('entity_relatives', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
            }); 
         } 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
