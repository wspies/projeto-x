<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Researchgroup extends Model
{
    protected $table = 'researchgroup';

	protected $fillable  = [
		   	'name',			
		   	'notes',
    	];

    public function diagnosis()
    {
        return $this->BelongsToMany('App\Diagnose');
    }

}
