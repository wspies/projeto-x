<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EntitySpecialNeed extends Model
{
    protected $table = 'entity_special_needs';

	public function diagnosis()
    {
        return $this->BelongsToMany('App\Diagnose');
    }

}
