<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EntityDsm extends Model
{
    protected $table = 'entity_dsm';

	public function diagnosis()
    {
    	return $this->belongsToMany('App\Diagnose');
    }

}
