<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diagnose extends Model
{
    protected $table = 'diagnose';

	protected $fillable  = [
		   	'patient_id',			
		   	'condition',
            'notes',
		   	'reseach_group'
    	];

	/**
     * Get the patient belonging to this diagnosis
     * Using the hasManyThrough relation
     * 
     * @return Collection of a patient
     */

    public function patient()
    {
    	return $this->belongsTo('App\Patient');
    }

  	/**
     * Get the dsm codes belonging to this diagnosis
     * 
     * @return Collection of dsm
     */
    public function dsm()
    {
    	return $this->BelongsToMany('App\EntityDsm');
    }
    
    public function specialNeeds()
    {
        return $this->BelongsToMany('App\EntitySpecialNeed');
    }

    public function relatives()
    {
        return $this->BelongsToMany('App\EntityRelative');
    }

    public function medication()
    {
        return $this->hasMany('App\DiagnoseMedicine');
    }

    public function researchgroups()
    {
        return $this->BelongsToMany('App\Researchgroup');
    }
}
