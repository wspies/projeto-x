<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LifestyleActivity extends Model
{
	protected $table = 'lifestyle_activity';

	protected $fillable  = [
			
			'lifestyle_id',
            'activity_description',
            'activity_weekly_frequency'            
    	];
}
