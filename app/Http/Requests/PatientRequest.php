<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PatientRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required',
            'lastname' => 'required',
            'address1' => 'required',
            'zipcode' => 'required',
            'city' => 'required',
            'telephone' => 'required|numeric',
            'mobile' => 'required|numeric',
            'email' => 'required|email',
            'urbanarea' => 'required',
            'birthday' => 'required|date'];
    }
}
