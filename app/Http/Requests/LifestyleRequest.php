<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class LifestyleRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'patient_id' => 'required',
            'vegetable_fruit' => 'required',
            'feijao' => 'required',
            'red_meat' => 'required',
            'whole_milk' => 'required',
            'physical_activity' => 'required',
            'commuting_activity' => 'required',
            'without_activity' => 'required',
            'tv_3_hours' => 'required',
            'regular_alcohol' => 'required',
            'bmi' => 'required',
            'smoker' => 'required'
            ];
    }

}
