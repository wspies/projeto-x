<?php

namespace App\Http\Controllers\Lifestyle;

use App\Http\Controllers\Controller;
use App\Http\Requests\LifestyleRequest;
use App\Lifestyle;
use App\LifestyleActivity;
use App\Patient;

use Illuminate\Http\Request;

class LifestyleController extends Controller
{
    

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create($patient_id)
    {
        $patient = Patient::findorFail($patient_id);
        return view('lifestyle.create',compact('patient'));
        
    }

    /**
     * Store a new lifestyle record.
     * if activities are specified create (multiple) lifestyleactivity records
     *
     * @param  LifestyleRequest $request
     * @return Response
     */
    public function store(LifestyleRequest $request)
    {
        $lifestyle = lifestyle::create($request->all());
        $patient = $lifestyle->patient;
        
        foreach($request->activity_description as $i=>$activity)
        {
            if(!empty(trim($activity))) {
                $lifestyleActivity = new LifestyleActivity();    
                $lifestyleActivity->lifestyle_id = $lifestyle->id;
                $lifestyleActivity->activity_description = $activity;
                $lifestyleActivity->activity_weekly_frequency = $request->activity_weekly_frequency[$i];
                $lifestyleActivity->save();            
            }
        }

        return redirect('/patient/'.$patient->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(Lifestyle $lifestyle)
    {
        // $patient = Patient::findorFail($lifestyle->patient_id);
        $patient = $lifestyle->patient;
         
        return view('lifestyle.lifestyle',compact('lifestyle'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Lifestyle  $lifestyle
     * @return Response
     */
    public function edit(Lifestyle $lifestyle)
    {
        return view('lifestyle.edit',compact('lifestyle'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  LifestyleRequest  $request
     * @param  Lifestyle  $lifestyle
     * @return Response
     */
    public function update(LifestyleRequest $request,Lifestyle $lifestyle)
    {
        $lifestyle->update($request->all());
               
        return redirect('/patients/'.$request->patient_id);  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(request $reqeust, Lifestyle $lifestyle)
    {
       
    }




}
