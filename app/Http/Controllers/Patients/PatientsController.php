<?php

namespace App\Http\Controllers\Patients;

use App\Http\Controllers\Controller;
use App\Http\Requests\PatientRequest;
use App\Diagnose;
use App\Lifestyle;
use App\LifestyleActivity;
use App\Patient;

use Illuminate\Http\Request;

class PatientsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $patients = Patient::all();
        return view('patients.overview',compact('patients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
         return view('patients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PatientRequest  $request
     * @return Response
     */
    public function store(PatientRequest $request)
    {
        $patient = patient::create($request->all());
        
        return redirect('/patients/'.$patient->id);
 
    }

    /**
     * Display the specified resource.
     *
     * @param  Patient  $patient
     * @return Response
     */
    public function show(Patient $patient)
    {
      // $activities = $patient->activities;
      // $diagnose = $patient->diagnose;
      // $medication = $patient->medication;
      // $dsm = $patient->Dsm();
      // $specialNeeds = $patient->specialNeeds();
      // $relatives = $patient->relatives();

      return view('patients.patient',compact('patient'));

        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Patient  $patient
     * @return Response
     */
    public function edit(Patient $patient)
    {
        return view('patients.edit',compact('patient'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PatientRequest  $request
     * @param  Patient  $patient
     * @return Response
     */
    public function update(PatientRequest $request, Patient $patient)
    {
        
        $patient->update($request->all());

        return redirect('/patients/'.$patient->id);        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Patient  $patient
     * @return Response
     */
    public function destroy(Patient $patient)
    {
        $patient->delete();
        $patients = Patient::all();

        return view('patients.patients',compact('patients'));
    }
}
