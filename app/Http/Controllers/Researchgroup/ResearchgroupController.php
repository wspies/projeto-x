<?php

namespace App\Http\Controllers\Researchgroup;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ResearchgroupRequest;

use App\Researchgroup;
use App\Http\Controllers\Controller;

class ResearchgroupController extends Controller
{
    

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $researchgroups = Researchgroup::all();
        
        return view('researchgroup.overview',compact('researchgroups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
         return view('researchgroup.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ResearchgroupRequest  $request
     * @return Response
     */
    public function store(ResearchgroupRequest $request)
    {
        researchgroup::create($request->all());
        
        return redirect('/researchgroup/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(Researchgroup $researchgroup)
    {
        return view('researchgroup.data',compact('researchgroup'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(Researchgroup $researchgroup)
    {
        return view('researchgroup.edit',compact('researchgroup'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(ResearchgroupRequest $request, Researchgroup $researchgroup)
    {
        $researchgroup->update($request->all());

        return redirect('/researchgroup/');        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Researchgroup $researchgroup)
    {
        $researchgroup->delete();
        $researchgroups = Researchgroup::all();

        return view('researchgroup.overview',compact('researchgroups'));
    }
}
