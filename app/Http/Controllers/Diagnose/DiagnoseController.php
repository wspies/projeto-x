<?php

namespace App\Http\Controllers\Diagnose;

use Illuminate\Http\Request;

use App\Http\Requests\DiagnoseRequest;
use App\Diagnose;
use App\DiagnoseMedicine;
use App\EntityDsm;
use App\EntityRelative;
use App\EntitySpecialNeed;
use App\Patient;
use App\Researchgroup;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class DiagnoseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     * @param  int  $patient_id
     * @return Response
     */
    public function create($patient_id)
    {
        $patient = Patient::findorFail($patient_id);
        $dsm = EntityDsm::lists('name','id');
        $specialNeeds = EntitySpecialNeed::lists('description','id');
        $relatives = EntityRelative::lists('name','id');
        $researchgroups = Researchgroup::lists('name','id');

        return view('diagnose.create',compact(['patient','dsm','specialNeeds','relatives','researchgroups']));   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  DiagnoseRequest  $request
     * @return Response
     */
    public function store(DiagnoseRequest $request)
    {
        $diagnose = Diagnose::create($request->all());
        $diagnose->dsm()->attach($request->input('dsm'));
        $diagnose->specialNeed()->attach($request->input('special_needs'));
        $diagnose->relatives()->attach($request->input('relatives'));
        $diagnose->researchgroups()->attach($request->input('researchgroups',[]));
        
        foreach($request->medicine_name as $i=>$medicine)
        {
            if(!empty(trim($medicine))) {
                $diagnoseMedicine = new DiagnoseMedicine();    
                $diagnoseMedicine->diagnose_id = $diagnose->id;
                $diagnoseMedicine->medicine_name = $medicine;
                $diagnoseMedicine->medicine_dose = $request->medicine_dose[$i];
                $diagnoseMedicine->medicine_observation = $request->medicine_observation[$i];
                $diagnoseMedicine->save();            
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(Diagnose $diagnose)
    {
        // fill the selectboxes
        $dsm = EntityDsm::lists('name','id');
        $specialNeeds = EntitySpecialNeed::lists('description','id');
        $relatives = EntityRelative::lists('name','id');
        $researchgroups = Researchgroup::lists('name','id');
        
        return view('diagnose.edit',compact(['diagnose',
                                              'dsm',
                                              'specialNeeds',
                                              'relatives',
                                              'researchgroups']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  DiagnoseRequest  $request
     * @param  Diagnose  $diagnose
     * @return Response
     */
    public function update(DiagnoseRequest $request, Diagnose $diagnose)
    {
        $diagnose->update($request->all());
        $diagnose->dsm()->sync($request->input('dsm',[]));
        $diagnose->specialNeeds()->sync($request->input('special_needs',[]));
        $diagnose->relatives()->sync($request->input('relatives',[]));
        $diagnose->researchgroups()->sync($request->input('researchgroups',[]));

        return redirect('/patients/'.$request->patient_id);  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
