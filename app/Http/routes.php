<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('administration', function() {
	return view('administration.index');
});

Route::get('dashboard', ['middleware' => 'auth','uses' => 'Dashboard\Dashboardcontroller@index']);

// Patient routes
Route::resource('patients','Patients\PatientsController');

// researchgroup
Route::resource('researchgroup','Researchgroup\ResearchgroupController');

// Lifestyle routes
Route::get('lifestyle/create/{patient_id}', 'Lifestyle\LifestyleController@create');
Route::resource('lifestyle','Lifestyle\LifestyleController');

//Diagnosis routes
Route::get('diagnose/create/{patient_id}', 'Diagnose\DiagnoseController@create');
Route::resource('diagnose','Diagnose\DiagnoseController');

//Exercise routes
Route::resource('excersise','Exercise\ExcersiseController');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);