<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiagnoseMedicine extends Model
{
	protected $table = 'diagnose_medicine';

	protected $fillable  = [
			
			'diagnose_id',
            'medicine_name',
            'medicine_dose',
            'medicine_observation'            
    	];
}
