<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EntityRelative extends Model
{
	protected $table = 'entity_relatives';

	public function diagnosis()
    {
        return $this->BelongsToMany('App\Diagnose');
    }
}
