<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lifestyle extends Model
{

	protected $table = 'lifestyle';

	protected $fillable  = [
		'patient_id',
            'vegetable_fruit',
            'feijao',
            'red_meat',
            'whole_milk',
            'physical_activity',
            'commuting_activity',
            'without_activity',
            'tv_3_hours',
            'regular_alcohol',
            'smoker',
            'smoker_sigaretes_day',
            'smoker_years'
    	];


 	/**
     * A lifestyle belongs to a patient
     * Using the belongsTo relations
     *
     * 
     * @return Collection of patient
     */
    public function patient()
    {
        return $this->belongsTo('App\Patient');
    }

    /**
     * Get all lifestyle activities connected to a lifestyle
     * Using the hasManyThrough relation
     *
     * 
     * @return Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function activities()
    {
        return $this->hasMany('App\LifestyleActivity');
    }
}
