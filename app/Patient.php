<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    //
    protected $table = 'patient';

    protected $fillable  = [
    	'firstname',
    	'middlename',
    	'lastname',
    	'birthday',
    	'identifier',
    	'address1',
    	'address2',
    	'zipcode',
    	'city',
    	'neighborhood',
    	'urbanarea',
    	'telephone',
    	'mobile',
    	'email',
    	'profession',
		'notes'
    	];


    public function formatFullName()
    {
        $name = $this->firstname;

        if (trim($this->middlename) != ''){
			$name .= ' ';
			$name .= $this->middlename;
        }

        $name .= ' ';
        $name .= $this->lastname;

        return $name;
    }

     public function lifestyle(){

        return $this->hasOne('App\Lifestyle');        
    }

    /**
     * Get all lifestyle activities for a patient
     * Using the hasManyThrough relation
     *
     * 
     * @return Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function activities()
    {
        return $this->hasManyThrough('App\LifestyleActivity', 'App\Lifestyle');
    }

    /**
     * Get the diagnose for a patient
     * Using the hasone relation
     *
     * 
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */

    public function diagnose()
    {
        return $this->hasOne('App\Diagnose');   
    }

    /**
     * Get the medication for a patient
     * Using the hasManyThrough relation
     *
     * 
     * @return Illuminate\Database\Eloquent\Relations\HasManyThrough
     */

    public function medication()
    {
        return $this->hasManyThrough('App\DiagnoseMedicine', 'App\Diagnose');
    }

    public function scopeDsm()
    {
        return Diagnose::find($this->diagnose->id)->dsm;
    }

    public function scopeSpecialNeeds()
    {
        return Diagnose::find($this->diagnose->id)->specialNeed;
    }

    public function scopeRelatives()
    {
        return Diagnose::find($this->diagnose->id)->relatives;
    }

   

    
}
