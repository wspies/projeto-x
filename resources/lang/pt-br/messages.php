<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'Password'       => 'Senha',
    'Email'          => 'Email',
    'Patient'        => 'Paciente',
    'Patients'        => 'Pacientes',
    'Register'        => 'Cadastrar',
    'Patient_list'        => 'Lista de pacientes',
    'Firstname'        => 'Primeiro nome',
    'Middlename'        => 'Nome do meio',
    'Lastname'        => 'Sobrenome',
    'Notes'        => 'Notas',
    'Birthday'        => 'Data de Nascimento',
    'Address'        => 'Endereço',
    'Neighborhood'        => 'Bairro',
    'City'        => 'Cidade',
    'Urban_area'        => 'Area Urbana',
    'Rural_area'        => 'Area Rural',
    'Telephone'        => 'Telefone',
    'Mobile'        => 'Celular',
    'Email'        => 'E-mail',
    'Profession'        => 'Profissão',
    'Education'        => 'Escolaridade'
];


