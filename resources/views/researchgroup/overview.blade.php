@extends('app')

@section('menu')
    @extends('dashboard.menu')
@endsection

@section('content')
<div class="container">
	<h1>Research group</h1>
	<hr/>
	<table class="table">
		<thead>
			<th>Group name</th>
			<th></th>
		</thead>
		@foreach($researchgroups as $researchgroup)
			<tr>
				<td>{{$researchgroup->name}}</td>
				
				<td>
					{!! Form::open(array('class' => 'inline-form', 'method' => 'DELETE', 'url' => '/researchgroup/'.$researchgroup->id)) !!}
					<a href="{{ URL::to('/researchgroup/'.$researchgroup->id)}}"><button type="button" class="btn btn-default inline-form"><span class="glyphicon glyphicon-open"></span></button></a>
					<a href="{{ URL::to('/researchgroup/'.$researchgroup->id).'/edit'}}"><button type="button" class="btn btn-default inline-form"><span class="glyphicon glyphicon-edit"></span></button></a>
                   	<button type="submit" class="btn btn-default inline-form"><span class="glyphicon glyphicon-remove"></span></button>
					{!! Form::close() !!}
				</td>
			</tr>
		@endforeach
	</table>
	
</div>
@endsection