@extends('app')

@section('menu')
    @extends('dashboard.menu')
@endsection

@section('content')
<div class="container">
	<h1>Create new research group</h1>
	<hr/>
	@include ('errors.list')
	{!! Form::open(['url' => '/researchgroup']) !!}
	
	@include ('researchgroup.form',['buttonText' => 'Register'])			

	{!! Form::close() !!}
</div>
@endsection