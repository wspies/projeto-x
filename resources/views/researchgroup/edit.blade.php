@extends('app')

@section('menu')
    @extends('dashboard.menu')
@endsection

@section('content')
<div class="container">
	<h1>Edit: {{$researchgroup->name }}</h1>
	<hr/>
	
	@include ('errors.list')

	{!! Form::model($researchgroup,['method' => 'PATCH', 'url' => '/researchgroup/'.$researchgroup->id]) !!}
	
	@include ('researchgroup.form',['buttonText' => 'Update'])			
	
	{!! Form::close() !!}
</div>

@endsection