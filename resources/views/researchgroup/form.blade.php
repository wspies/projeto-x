<div class="form-group">
	{!! Form::label ('name',"Name:") !!}
	{!! Form::text ('name',null,['class' => 'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label ('notes',"Notes:") !!}
	{!! Form::textarea ('notes',null,['class' => 'form-control']) !!}
</div>			
<div class="form-group">
	{!! Form::submit ($buttonText,['class' => 'btn btn-success from-control']) !!}
</div>	