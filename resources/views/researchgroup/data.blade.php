@extends('app')

@section('menu')
    @extends('dashboard.menu')
@endsection

@section('content')
<div class="container">
	<h1>{{ $researchgroup->name }}</h1>
	<hr/>
	<div class="col-md-6">
		
		<div class="form-group">
		    <label class="col-sm-7 control-label">Notes:</label>
		    <div class="col-sm-5, inline-form">
				<p class="form-control-static">{{ $researchgroup->notes }}</p>
		    </div>
		</div>	
	</div>
</div>
@endsection