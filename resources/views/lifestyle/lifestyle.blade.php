@extends('app')

@section('menu')
    @extends('dashboard.menu')
@endsection

@section('content')
<div class="container">
	<h1>Lifestyle {{ $lifestyle->patient->formatFullName() }}</h1>
	<hr/>
	@include('lifestyle.data')
</div>
@endsection