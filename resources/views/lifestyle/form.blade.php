		
	{!! Form::hidden ('patient_id',$lifestyle->patient->id,['class' => 'form-control']) !!}
				
	<div class="form-group">
		{!! Form::label ('',"Consumes vegetables and fruits regularly:") !!}
		<br/>
		{!! Form::radio ('vegetable_fruit','1',null,['id' => 'vegetable_fruit_yes']) !!}
		{!! Form::label ('vegetable_fruit_yes',"Yes",['style' => 'width:50px;']) !!}
		{!! Form::radio ('vegetable_fruit','0',null,['id' => 'vegetable_fruit_no']) !!}
		{!! Form::label ('vegetable_fruit_no',"No") !!}
	</div>

	<div class="form-group">
		{!! Form::label ('',"Consumes feijao regularly:") !!}
		<br/>
		{!! Form::radio ('feijao','1',null,['id' => 'feijao_yes']) !!}
		{!! Form::label ('feijao_yes',"Yes",['style' => 'width:50px;']) !!}
		{!! Form::radio ('feijao','0',null,['id' => 'feijao_no']) !!}
		{!! Form::label ('feijao_no',"No") !!}
	</div>

	<div class="form-group">
		{!! Form::label ('',"Consumes red meat regularly:") !!}
		<br/>
		{!! Form::radio ('red_meat','1',null,['id' => 'red_meat_yes']) !!}
		{!! Form::label ('red_meat_yes',"Yes",['style' => 'width:50px;']) !!}
		{!! Form::radio ('red_meat','0',null,['id' => 'red_meat_no']) !!}
		{!! Form::label ('red_meat_no',"No") !!}
	</div>

	<div class="form-group">
		{!! Form::label ('',"Consumes whole milk regularly:") !!}
		<br/>
		{!! Form::radio ('whole_milk','1',null,['id' => 'whole_milk_yes']) !!}
		{!! Form::label ('whole_milk_yes',"Yes",['style' => 'width:50px;']) !!}
		{!! Form::radio ('whole_milk','0',null,['id' => 'whole_milk_no']) !!}
		{!! Form::label ('whole_milk_no',"No") !!}
	</div>

	<div class="form-group">
		{!! Form::label ('',"Physical activity:") !!}
		<br/>
		{!! Form::radio ('physical_activity','1',null,['id' => 'physical_activity_yes']) !!}
		{!! Form::label ('physical_activity_yes',"Yes",['style' => 'width:50px;']) !!}
		{!! Form::radio ('physical_activity','0',null,['id' => 'physical_activity_no']) !!}
		{!! Form::label ('physical_activity_no',"No") !!}

		@if ($lifestyle->physical_activity == 1)
			@foreach ($lifestyle->activities as $activity)
				<div class="form-group">
				{!! Form::label ('activity_description',"Activity:") !!}
				{!! Form::text ('activity_description[]',$activity->activity_description,['class' => 'form-control,form-inline']) !!}
				{!! Form::label ('activity_weekly_frequency',"Weekly frequency:") !!}
				{!! Form::text ('activity_weekly_frequency[]',$activity->activity_weekly_frequency,['class' => 'form-control,form-inline','size' =>'5']) !!}
				</div>
			@endforeach
		@endif
	</div>
	
	<div class="form-group">
		{!! Form::label ('',"Commuting activity:") !!}
		<br/>
		{!! Form::radio ('commuting_activity','1',null,['id' => 'commuting_activity_yes']) !!}
		{!! Form::label ('commuting_activity_yes',"Yes",['style' => 'width:50px;']) !!}
		{!! Form::radio ('commuting_activity','0',null,['id' => 'commuting_activity_no']) !!}
		{!! Form::label ('commuting_activity_no',"No") !!}
	</div>

	<div class="form-group">
		{!! Form::label ('',"Without any activity:") !!}
		<br/>
		{!! Form::radio ('without_activity','1',null,['id' => 'without_activity_yes']) !!}
		{!! Form::label ('without_activity_yes',"Yes",['style' => 'width:50px;']) !!}
		{!! Form::radio ('without_activity','0',null,['id' => 'without_activity_no']) !!}
		{!! Form::label ('without_activity_no',"No") !!}
	</div>
	
	<div class="form-group">
		{!! Form::label ('',"More then 3 hours of tv every day:") !!}
		<br/>
		{!! Form::radio ('tv_3_hours','1',null,['id' => 'tv_3_hours_yes']) !!}
		{!! Form::label ('tv_3_hours_yes',"Yes",['style' => 'width:50px;']) !!}
		{!! Form::radio ('tv_3_hours','0',null,['id' => 'tv_3_hours_no']) !!}
		{!! Form::label ('tv_3_hours_no',"No") !!}
	</div>

	<div class="form-group">
		{!! Form::label ('',"Regular alcohol consumer:") !!}
		<br/>
		{!! Form::radio ('regular_alcohol','1',null,['id' => 'regular_alcohol_yes']) !!}
		{!! Form::label ('regular_alcohol_yes',"Yes",['style' => 'width:50px;']) !!}
		{!! Form::radio ('regular_alcohol','0',null,['id' => 'regular_alcohol_no']) !!}
		{!! Form::label ('regular_alcohol_no',"No") !!}
	</div>

	<div class="form-group">
		{!! Form::label ('bmi',"BMI:") !!}
		{!! Form::text ('bmi',null,['class' => 'form-control','size' => 5]) !!}
	</div>

	<div class="form-group">
		{!! Form::label ('',"Smoker:") !!}
		<br/>
		{!! Form::radio ('smoker','Y',null,['id' => 'smoker_yes']) !!}
		{!! Form::label ('smoker_yes',"Yes",['style' => 'width:50px;']) !!}
		{!! Form::radio ('smoker','N',null,['id' => 'smoker_no']) !!}
		{!! Form::label ('smoker_no',"No",['style' => 'width:50px;']) !!}
		{!! Form::radio ('smoker','E',null,['id' => 'smoker_ex']) !!}
		{!! Form::label ('smoker_ex',"Ex smoker") !!}
	</div>

	<div class="form-group">
		{!! Form::label ('smoker_sigaretes_day',"Sigaretes by day:") !!}
		{!! Form::text ('smoker_sigaretes_day',null,['class' => 'form-control,form-inline']) !!}
		{!! Form::label ('smoker_years',"How many years:") !!}
		{!! Form::text ('smoker_years',null,['class' => 'form-control,form-inline','size' =>'5']) !!}
	</div>

	<div class="form-group">
		{!! Form::submit ($buttonText,['class' => 'btn btn-success from-control']) !!}
	</div>	
