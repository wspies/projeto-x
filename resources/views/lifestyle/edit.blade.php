@extends('app')

@section('menu')
    @extends('dashboard.menu')
@endsection

@section('content')
<div class="container">
	<h1>Lifestyle: {{ $lifestyle->patient->formatFullName() }}</h1>
	<hr/>
	
	@include ('errors.list')

	{!! Form::model($lifestyle,['method' => 'PATCH', 'url' => '/lifestyle/'.$lifestyle->patient->id]) !!}
	
	@include ('lifestyle.form',['buttonText' => 'Update'])			
	
	{!! Form::close() !!}
</div>

@endsection