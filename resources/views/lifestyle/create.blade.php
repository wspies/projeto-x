@extends('app')

@section('menu')
    @extends('dashboard.menu')
@endsection

@section('content')
<div class="container">
	<h1>Lifestyle {{ $patient->formatFullName() }}</h1>
	<hr/>
	@include ('errors.list')
	{!! Form::open(['url' => '/lifestyle']) !!}
	
	@include ('lifestyle.form',['buttonText' => 'Register'])			

	{!! Form::close() !!}
</div>
@endsection