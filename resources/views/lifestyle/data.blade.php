<div class="col-md-6">
	<h3><a href=/lifestyle/{{ $patient->lifestyle->id }}/edit >Lifestyle</a></h3>
	
	<div class="form-group">
	    <label class="col-sm-7 control-label">Consumes vegetables and fruits regularly:</label>
	    <div class="col-sm-5, inline-form">
	      <p class="form-control-static">{{ $patient->lifestyle->vegetable_fruit }}</p>
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-sm-7 control-label">Consumes feijao regularly:</label>
	    <div class="col-sm-5, inline-form">
	      <p class="form-control-static">{{ $patient->lifestyle->feijao }}</p>
	    </div>
	</div>	
	<div class="form-group">
	    <label class="col-sm-7 control-label">Consumes red meat regularly:</label>
	    <div class="col-sm-5, inline-form">
	      <p class="form-control-static">{{ $patient->lifestyle->red_meat }}</p>
	    </div>
	</div>	
	<div class="form-group">
	    <label class="col-sm-7 control-label">Consumes whole milk regularly:</label>
	    <div class="col-sm-5, inline-form">
	      <p class="form-control-static">{{ $patient->lifestyle->whole_milk }}</p>
	    </div>
	</div>	
	<div class="form-group">
	    <label class="col-sm-7 control-label">Physical activity:</label>
	    <div class="col-sm-5, inline-form">
	      <p class="form-control-static">{{ $patient->lifestyle->physical_activity }}</p>
	    </div>
	</div>
	@if ($patient->lifestyle->physical_activity == 1)
		<div class="form-group" style="padding-left:15px;">
			<table class="table">
				<th>Activity</th>
				<th>Weekly frequency</th>
				@foreach ($patient->activities as $activity)
					<tr>
						<td>{{ $activity->activity_description}}</td>
						<td>{{ $activity->activity_weekly_frequency}}</td>
					</tr>
				@endforeach
			</table>
		</div>	
	@endif
	<div class="form-group">
	    <label class="col-sm-7 control-label">Commuting activity:</label>
	    <div class="col-sm-5, inline-form">
	      <p class="form-control-static">{{ $patient->lifestyle->commuting_activity }}</p>
	    </div>
	</div>	
	<div class="form-group">
	    <label class="col-sm-7 control-label">Without any activity:</label>
	    <div class="col-sm-5, inline-form">
	      <p class="form-control-static">{{ $patient->lifestyle->without_activity }}</p>
	    </div>
	</div>	
	<div class="form-group">
	    <label class="col-sm-7 control-label">More then 3 hours of tv every day:</label>
	    <div class="col-sm-5, inline-form">
	      <p class="form-control-static">{{ $patient->lifestyle->tv_3_hours }}</p>
	    </div>
	</div>	
	<div class="form-group">
	    <label class="col-sm-7 control-label">Regular alcohol consumer:</label>
	    <div class="col-sm-5, inline-form">
	      <p class="form-control-static">{{ $patient->lifestyle->regular_alcohol }}</p>
	    </div>
	</div>	
	<div class="form-group">
	    <label class="col-sm-7 control-label">bmi:</label>
	    <div class="col-sm-5, inline-form">
	      <p class="form-control-static">{{ $patient->lifestyle->bmi }}</p>
	    </div>
	</div>	
	<div class="form-group">
	    <label class="col-sm-7 control-label">Smoker:</label>
	    <div class="col-sm-5, inline-form">
	      <p class="form-control-static">{{ $patient->lifestyle->smoker }}</p>
	    </div>
	</div>	
	@if ($patient->lifestyle->smoker == 'Y')
		<div class="form-group">
		    <label class="col-sm-7 control-label">Sigarets a day:</label>
		    <div class="col-sm-5, inline-form">
		      <p class="form-control-static">{{ $patient->lifestyle->smoker_sigaretes_day }}</p>
		    </div>
		</div>	
		<div class="form-group">
		    <label class="col-sm-7 control-label">Smoker years:</label>
		    <div class="col-sm-5, inline-form">
		      <p class="form-control-static">{{ $patient->lifestyle->smoker_years }}</p>
		    </div>
		</div>	
	@endif
</div>