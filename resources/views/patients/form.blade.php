	<div class="form-group">
			{!! Form::label ('firstname',trans('messages.Firstname')) !!}
			{!! Form::text ('firstname',null,['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::label ('middlename',trans('messages.Middlename')) !!}
			{!! Form::text ('middlename',null,['class' => 'form-control']) !!}
		</div>		
		<div class="form-group">
			{!! Form::label ('lastname',trans('messages.Lastname')) !!}
			{!! Form::text ('lastname',null,['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::label ('birthday',trans('messages.Birthday')) !!}
			{!! Form::input ('date','birthday',null,['class' => 'form-control']) !!}
		</div>		
		<div class="form-group">
			{!! Form::label ('address1',trans('messages.Address')." 1") !!}
			{!! Form::text ('address1',null,['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::label ('address2',trans('messages.Address')." 2") !!}
			{!! Form::text ('address2',null,['class' => 'form-control']) !!}
		</div>		
		<div class="form-group">
			{!! Form::label ('neighborhood',trans('messages.Neighborhood')) !!}
			{!! Form::text ('neighborhood',null,['class' => 'form-control']) !!}
		</div>		
		<div class="form-group">
			{!! Form::label ('city',trans('messages.City')) !!}
			{!! Form::text ('city',null,['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::label ('zipcode',trans('messages.City')) !!}
			{!! Form::text ('zipcode',null,['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::label ('urbanarea',trans('messages.Urban_area')) !!}
			{!! Form::radio ('urbanarea','1',null) !!}
			{!! Form::label ('rural',trans('messages.Rural_area')) !!}
			{!! Form::radio ('urbanarea','0',null,['id' => 'rural']) !!}
		</div>
		<div class="form-group">
			{!! Form::label ('telephone',trans('messages.Telephone')) !!}
			{!! Form::text ('telephone',null,['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::label ('mobile',trans('messages.Mobile')) !!}
			{!! Form::text ('mobile',null,['class' => 'form-control']) !!}
		</div>		
		<div class="form-group">
			{!! Form::label ('email',trans('messages.Email')) !!}
			{!! Form::text ('email',null,['class' => 'form-control']) !!}
		</div>	
		<div class="form-group">
			{!! Form::label ('profession',trans('messages.Profession')) !!}
			{!! Form::text ('profession',null,['class' => 'form-control']) !!}
		</div>		
		<div class="form-group">
			{!! Form::label ('education',trans('messages.Education')) !!}
			{!! Form::text ('education',null,['class' => 'form-control']) !!}
		</div>		
		<div class="form-group">
			{!! Form::label ('notes',trans('messages.Notes')) !!}
			{!! Form::textarea ('notes',null,['class' => 'form-control']) !!}
		</div>			

		<div class="form-group">
			{!! Form::submit ($buttonText,['class' => 'btn btn-success from-control']) !!}
		</div>	