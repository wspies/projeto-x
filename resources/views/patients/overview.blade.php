@extends('app')

@section('menu')
    @extends('dashboard.menu')
@endsection

@section('content')
<div class="container">
	<h1>{{ trans('messages.Patients') }}</h1>
	<hr/>
	<table class="table">
		<thead>
			<th>{{ trans('messages.Firstname') }}</th>
			<th>{{ trans('messages.Middlename') }}</th>
			<th>{{ trans('messages.Lastname') }}</th>
			<th></th>
		</thead>
		
		@foreach($patients as $patient)
		<patient>
			<tr>
				<td>{{$patient->firstname}}</td>
				<td>{{$patient->middlename}}</td>
				<td>{{$patient->lastname}}</td>
				<td>
					{!! Form::open(array('class' => 'inline-form', 'method' => 'DELETE', 'url' => '/patients/'.$patient->id)) !!}
					<a href="{{ URL::to('patients/'.$patient->id)}}"><button type="button" class="btn btn-default inline-form"><span class="glyphicon glyphicon-open"></span></button></a>
					<a href="{{ URL::to('patients/'.$patient->id).'/edit'}}"><button type="button" class="btn btn-default inline-form"><span class="glyphicon glyphicon-edit"></span></button></a>
                   	<button type="submit" class="btn btn-default inline-form"><span class="glyphicon glyphicon-remove"></span></button>
					{!! Form::close() !!}
				</td>
			</tr>


		</patient>
		@endforeach
	</table>
	
</div>
@endsection