@extends('app')

@section('menu')
    @extends('dashboard.menu')
@endsection

@section('content')
<div class="container">
	<h1>{{ trans('messages.Register') }} {{ trans('messages.Patient') }}</h1>
	<hr/>
	@include ('errors.list')
	{!! Form::open(['url' => '/patients']) !!}
	
	@include ('patients.form',['buttonText' => trans('messages.Register')])			

	{!! Form::close() !!}
</div>
@endsection