@extends('app')

@section('menu') 
    @extends('dashboard.menu')
@endsection

@section('content')
<div class="container">
	<h1>{{ $patient->formatFullName() }}</h1>
	@if(is_null($patient->lifestyle))
		<a href=/lifestyle/create/{{ $patient->id }} class="btn btn-primary">Register lifestyle</a>
	@endif
	@if(is_null($patient->diagnose))
		<a href="/diagnose/create/{{ $patient->id }}" class="btn btn-primary">Register diagnostic</a>
	@endif
	<hr/> 
	<patient>
		<div class="col-md-6">
			<h3><a href="/patients/{{ $patient->id.'/edit' }}">Personal Info</a></h3>
			<form class="form-horizontal">
				<div>
				    <label class="col-sm-2 control-label">{{ trans('messages.Firstname') }}</label>
				    <div class="col-sm-10">
				      <p class="form-control-static">{{ $patient->firstname }}</p>
				    </div>
		  		</div>
			  	<div class="form-group">
				    <label class="col-sm-2 control-label">{{ trans('messages.Middlename') }}</label>
				    <div class="col-sm-10">
				      <p class="form-control-static">{{ $patient->middlename }}</p>
				    </div>
		  		</div>
		  		<div class="form-group">
				    <label class="col-sm-2 control-label">{{ trans('messages.Lastname') }}</label>
				    <div class="col-sm-10">
				      <p class="form-control-static">{{ $patient->lastname }}</p>
				    </div>
		  		</div>
		  		<div class="form-group">
				    <label class="col-sm-2 control-label">{{ trans('messages.Notes') }}</label>
				    <div class="col-sm-10">
				      <p class="form-control-static">{{ $patient->notes }}</p>
				    </div>
				</div>
				<div class="form-group">
				    <label class="col-sm-2 control-label">{{ trans('messages.Birthday') }}</label>
				    <div class="col-sm-10">
				      <p class="form-control-static">{{ $patient->birthday }}</p>
				    </div>
		  		</div>
				<div class="form-group">
				    <label class="col-sm-2 control-label">{{ trans('messages.Address') }} 1</label>
				    <div class="col-sm-10">
				      <p class="form-control-static">{{ $patient->address1 }}</p>
				    </div>
		  		</div>
		  		<div class="form-group">
		  		    <label class="col-sm-2 control-label">{{ trans('messages.Address') }} 2</label>
		  		    <div class="col-sm-10">
		  		      <p class="form-control-static">{{ $patient->address2 }}</p>
		  		    </div>
		  		</div>
		  			
				<div class="form-group">
				    <label class="col-sm-2 control-label">{{ trans('messages.Neighborhood') }}</label>
				    <div class="col-sm-10">
				      <p class="form-control-static">{{ $patient->neighborhood }}</p>
				    </div>
		  		</div>
				<div class="form-group">
		  		    <label class="col-sm-2 control-label">{{ trans('messages.City') }}</label>
		  		    <div class="col-sm-10">
		  		      <p class="form-control-static">{{ $patient->city }}</p>
		  		    </div>
		  		</div>
		  		<div class="form-group">
		  		    <label class="col-sm-2 control-label">{{ trans('messages.Zipcode') }}</label>
		  		    <div class="col-sm-10">
		  		      <p class="form-control-static">{{ $patient->zipcode }}</p>
		  		    </div>
		  		</div>
				<div class="form-group">
				    <label class="col-sm-2 control-label">{{ trans('messages.Urban_area') }}</label>
				    <div class="col-sm-10">
				      <p class="form-control-static">{{  $patient->urbanarea }}</p>
				    </div>
				</div>
				<div class="form-group">
		  		    <label class="col-sm-2 control-label">{{ trans('messages.Telephone') }}</label>
		  		    <div class="col-sm-10">
		  		      <p class="form-control-static">{{  $patient->telephone }}</p>
		  		    </div>
		  		</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">{{ trans('messages.Mobile') }}</label>
					<div class="col-sm-10">
				  		<p class="form-control-static">{{ $patient->mobile }}</p>
					</div>
				</div>
				<div class="form-group">
				    <label class="col-sm-2 control-label">{{ trans('messages.Email') }}</label>
				    <div class="col-sm-10">
				      <p class="form-control-static">{{ $patient->email }}</p>
				    </div>
				</div>
				<div class="form-group">
				    <label class="col-sm-2 control-label">{{ trans('messages.Profession') }}</label>
				    <div class="col-sm-10">
				      <p class="form-control-static">{{ $patient->profession }}</p>
				    </div>
				</div>	
				<div class="form-group">
				    <label class="col-sm-2 control-label">{{ trans('messages.Education') }}</label>
				    <div class="col-sm-10">
				      <p class="form-control-static">{{ $patient->Education }}</p>
				    </div>
				</div>
				
			</form>
		</div>
		
		@if(!is_null($patient->lifestyle))
			@include('lifestyle.data')
		@endif 
		
		@if(!is_null($patient->diagnose))
			@include('diagnose.data')
		@endif 
	</patient>
	
</div>
@endsection