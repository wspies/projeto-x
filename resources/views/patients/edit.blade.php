@extends('app')

@section('menu')
    @extends('dashboard.menu')
@endsection

@section('content')
<div class="container">
	<h1>Edit: {{$patient->formatFullName() }}</h1>
	<hr/>
	
	@include ('errors.list')

	{!! Form::model($patient,['method' => 'PATCH', 'url' => '/patients/'.$patient->id]) !!}
	
	@include ('patients.form',['buttonText' => 'Update'])			
	
	{!! Form::close() !!}
</div>

@endsection