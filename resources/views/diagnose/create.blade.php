@extends('app')

@section('menu')
    @extends('dashboard.menu')
@endsection

@section('content')
<div class="container">
	<h1>Diagnostic {{ $patient->formatFullName() }}</h1>
	<hr/>
	@include ('errors.list')
	{!! Form::open(['url' => '/diagnose']) !!}
	
	@include ('diagnose.form',['buttonText' => 'Register'])			

	{!! Form::close() !!}
</div>
@endsection