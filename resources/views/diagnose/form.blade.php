		
	{!! Form::hidden ('patient_id',$diagnose->patient->id,['class' => 'form-control']) !!}
				
	<div class="form-group">
		{!! Form::label ('condition',"Condition:") !!}
		{!! Form::text ('condition',null,['class' => 'form-control']) !!}
	</div>

	<div class="form-group">
		{!! Form::label ('dsm',"DSM-V:") !!}
		{!! Form::select ('dsm[]',$dsm, $diagnose->dsm->lists('id')->toArray(),['class' => 'form-control','multiple']) !!}
	</div>

	<div class="form-group">
		{!! Form::label ('notes',"Notes:") !!}
		{!! Form::textarea ('notes',null,['class' => 'form-control']) !!}
	</div>			
	
	<div class="form-group">
		{!! Form::label ('special_needs',"Special needs:") !!}
		{!! Form::select ('special_needs[]',$specialNeeds, $diagnose->specialNeeds->lists('id')->toArray(),['class' => 'form-control','multiple']) !!}
	</div>	

	<div class="form-group">
		{!! Form::label ('relatives',"Family history:") !!}
		{!! Form::select ('relatives[]',$relatives, $diagnose->relatives->lists('id')->toArray(),['class' => 'form-control','multiple']) !!}
	</div>	
		
	<div class="form-group">
			@foreach ($diagnose->medication as $medicine)
				<div class="form-group">
					{!! Form::label ('medicine_name',"Medicine:") !!}
					{!! Form::text ('medicine_name[]',$medicine->medicine_name,['class' => 'form-control,form-inline']) !!}
					{!! Form::label ('medicine_dose',"dosage:") !!}
					{!! Form::text ('medicine_dose[]',$medicine->medicine_dose,['class' => 'form-control,form-inline','size' =>'5']) !!}
					{!! Form::label ('medicine_observation',"Observation:") !!}
					{!! Form::text ('medicine_observation[]',$medicine->medicine_observation,['class' => 'form-control,form-inline','size' =>'30']) !!}
				</div>
			@endforeach
			{!! Form::label ('medicine_name',"Medicine:") !!}
			{!! Form::text ('medicine_name[]',null,['class' => 'form-control,form-inline']) !!}
			{!! Form::label ('medicine_dose',"dosage:") !!}
			{!! Form::text ('medicine_dose[]',null,['class' => 'form-control,form-inline','size' =>'5']) !!}
			{!! Form::label ('medicine_observation',"Observation:") !!}
			{!! Form::text ('medicine_observation[]',null,['class' => 'form-control,form-inline','size' =>'30']) !!}
	</div>

	<div class="form-group">
		{!! Form::label ('researchgroups',"Research group:") !!}
		{!! Form::select ('researchgroups[]',$researchgroups, $diagnose->researchgroups->lists('id')->toArray(),['class' => 'form-control','multiple']) !!}
	</div>	
	
	<div class="form-group">
		{!! Form::submit ($buttonText,['class' => 'btn btn-success from-control']) !!}
	</div>	
	