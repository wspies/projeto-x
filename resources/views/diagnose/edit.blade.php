@extends('app')

@section('menu')
    @extends('dashboard.menu')
@endsection

@section('content')
<div class="container">
	<h1>Diagnose: {{$diagnose->patient->formatFullName() }}</h1>
	<hr/>
	
	@include ('errors.list')

	{!! Form::model($diagnose,['method' => 'PATCH', 'url' => '/diagnose/'.$diagnose->id]) !!}
	
	@include ('diagnose.form',['buttonText' => 'Update'])			
	
	{!! Form::close() !!}
</div>

@endsection