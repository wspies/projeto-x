<div class="col-md-6">
	<h3><a href=/diagnose/{{ $patient->diagnose->id }}/edit >Diagnose</a></h3>

	<div class="form-group">
	    <label class="col-sm-7 control-label">Condition:</label>
	    <div class="col-sm-5, inline-form">
			<p class="form-control-static">{{ $patient->diagnose->condition }}</p>
	    </div>
	</div>	

	<div class="form-group">
	    <label class="col-sm-7 control-label">DSM-V:</label>
	    <div class="col-sm-5, inline-form">
		@if (!is_null($patient->diagnose->dsm))  
			@foreach ($patient->diagnose->dsm as $ds)
			{{ $ds->code.'-'.$ds->name}}<br/>
			@endforeach    
		@endif
	    </div>
	</div>

	<div class="form-group">
	    <label class="col-sm-7 control-label">Notes:</label>
	    <div class="col-sm-5, inline-form">
	      <p class="form-control-static">{{ $patient->diagnose->notes }}</p>
	    </div>
	</div>

	<div class="form-group">
	    <label class="col-sm-7 control-label">Special needs:</label>
	    <div class="col-sm-5, inline-form">
		@if (!is_null($patient->diagnose->specialNeeds))  
			@foreach ($patient->diagnose->specialNeeds as $need)
			{{ $need->description}} <br/>
			@endforeach    
		@endif
	    </div>
	</div>

	<div class="form-group">
	    <label class="col-sm-7 control-label">Family history:</label>
	    <div class="col-sm-5, inline-form">
		@if (!is_null($patient->diagnose->relatives))  
			@foreach ($patient->diagnose->relatives as $relative)
			{{ $relative->name}}<br/>

			@endforeach    
		@endif
	    </div>
	</div>
	
	<div class="form-group">
	    <label class="col-sm-7 control-label">Medication:</label>
	</div>

	@if (!is_null($patient->diagnose->medication))
		<div class="form-group" style="padding-left:15px;">
			<table class="table">
				<th>Name</th>
				<th>Dosage</th>
				<th>Observation</th>
					@foreach ($patient->diagnose->medication as $medicine)
						<tr>
							<td>{{ $medicine->medicine_name }}</td>
							<td>{{ $medicine->medicine_dose}}</td>
							<td>{{ $medicine->medicine_observation}}</td>
						</tr>
					@endforeach
			</table>
		</div>	
	@endif

	<div class="form-group">
	    <label class="col-sm-7 control-label">Research group:</label>
	    <div class="col-sm-5, inline-form">
		@if (!is_null($patient->diagnose->researchgroups))  
			@foreach ($patient->diagnose->researchgroups as $researchgroup)
			{{ $researchgroup->name}}<br/>
			@endforeach    
		@endif
	    </div>
	</div>

</div>